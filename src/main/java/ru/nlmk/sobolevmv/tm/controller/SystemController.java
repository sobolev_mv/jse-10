package ru.nlmk.sobolevmv.tm.controller;

public class SystemController {

  public int displayError() {
    System.out.println("Error! Unknown program argument...");
    return -1;
  }

  public void displayWelcome() {
    System.out.println("** WELCOME TO TASK MANAGER **");
  }

  public int displayHelp() {
    System.out.println("version - display application version");
    System.out.println("about - display developer info");
    System.out.println("help - display list of commands");
    System.out.println("exit - terminate console application");
    System.out.println();
    System.out.println("project-create - create new project by name");
    System.out.println("project-clear - remove all project");
    System.out.println("project-list - display list of projects");
    System.out.println("project-view-by-index - display project by index");
    System.out.println("project-view-by-id - display project by id");
    System.out.println("project-remove-by-name - delete project by name");
    System.out.println("project-remove-by-id - delete project by id");
    System.out.println("project-remove-by-index - delete project by index");
    System.out.println("project-update-by-index - update project by index");
    System.out.println("project-update-by-id - update project by id");
    System.out.println();
    System.out.println("task-create - create new task by name");
    System.out.println("task-clear - remove all task");
    System.out.println("task-list - display list of tasks");
    System.out.println("task-view-by-index - display project by index");
    System.out.println("task-view-by-id - display project by id");
    System.out.println("task-remove-by-name - delete task by name");
    System.out.println("task-remove-by-id - delete task by id");
    System.out.println("task-remove-by-index - delete task by index");
    System.out.println("task-update-by-index - update task by index");
    System.out.println("task-update-by-id - update project by id");
    System.out.println("task-list-by-project-id - Display task list by project id");
    System.out.println("task-add-to-project-by-ids - Add task to project by ids");
    System.out.println("task-remove-form-project-by-ids - Remove task from project by id");
    System.out.println();
    return 0;
  }

  public int displayAbout() {
    System.out.println("Mikhail Sobolev");
    System.out.println("sobolev_mv@nlmk.com");
    return 0;
  }

  public int displayVersion() {
    System.out.println("1.0.0");
    return 0;
  }

  public int exit() {
    System.exit(0);
    return 0;
  }

}
