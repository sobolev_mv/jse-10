package ru.nlmk.sobolevmv.tm.constant;

public final class TerminalConst {

    public static final String CMD_HELP = "help";
    public static final String CMD_VERSION = "version";
    public static final String CMD_ABOUT = "about";
    public static final String CMD_EXIT = "exit";

    public static final String CMD_PROJECT_CREATE = "project-create";
    public static final String CMD_PROJECT_CLEAR = "project-clear";
    public static final String CMD_PROJECT_LIST = "project-list";
    public static final String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    public static final String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-e";
    public static final String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";
    public static final String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    public static final String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";
    public static final String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    public static final String CMD_TASK_CREATE = "task-create";
    public static final String CMD_TASK_CLEAR = "task-clear";
    public static final String CMD_TASK_LIST = "task-list";
    public static final String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";
    public static final String CMD_TASK_VIEW_BY_ID = "task-view-by-id";
    public static final String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";
    public static final String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";
    public static final String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    public static final String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";
    public static final String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";
    public static final String CMD_TASK_LIST_BY_PROJECT_ID = "task-list-by-project-id";
    public static final String CMD_TASK_ADD_TO_PROJECT_BY_IDS = "task-add-to-project-by-ids";
    public static final String CMD_TASK_REMOVE_FROM_PROJECT_BY_IDS = "task-remove-form-project-by-ids";

}
